### Zadanie

uruchomienie

```buildoutcfg
docker-compose build
docker-compose up
```

### API

* http://18.224.34.32/api/v1/

#### Dokumentacja
* http://18.224.34.32/api/v1/docs/

#####Django Admin
* http://18.224.34.32/api/v1/admin/
* login: admin
* hasło: 12345

### Frontend

* http://18.224.34.32
