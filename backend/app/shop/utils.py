from .models import Cart


def get_current_cart(request):
    session = get_or_create_session(request)
    cart, created = Cart.objects.get_or_create(session_key=session.session_key, pending=False)
    return cart


def get_or_create_session(request):
    if request.session is None or request.session.session_key is None:
        request.session.create()
        return request.session
    return request.session
