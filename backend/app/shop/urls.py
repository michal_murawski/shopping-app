from django.urls import path, re_path

from rest_framework.routers import DefaultRouter

from .views import CartAPIView, CartItemsViewSet, DocsView, OrderViewSet, ProductViewSet, PromoCodesViewSet

app_name = "shop"

router = DefaultRouter()

router.register(r'products', ProductViewSet)
router.register(r'orders', OrderViewSet)
router.register(r'cart/items', CartItemsViewSet)
router.register(r'cart/promocodes', PromoCodesViewSet)

urlpatterns = router.urls

urlpatterns += [
    path('docs', DocsView.as_view()),
    re_path(r'^cart/$', CartAPIView.as_view())
]
