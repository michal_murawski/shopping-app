from uuid import uuid4

from django.conf import settings
from django.db.models import Max
from django.utils.timezone import now


def apply_promo_code(obj):
    if obj.cart.first().validated_promo_codes.count():
        promo_codes = obj.cart.first().validated_promo_codes.filter(category__in=obj.product.category.all())
        if promo_codes:
            max_value = promo_codes.aggregate(Max('value'))['value__max']
            return float(obj.product.price) * (1 - (max_value / 100))
    return obj.product.price


def get_promo_code_expire_date():
    return now() + settings.PROMO_CODE_TIME_PERIOD


def create_promo_code():
    return uuid4().hex
