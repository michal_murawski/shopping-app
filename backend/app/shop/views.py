from django.views.generic import TemplateView

from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.mixins import CreateModelMixin, DestroyModelMixin
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet

from .models import Cart, CartItem, Order, Product, PromoCode
from .serializers import CartItemSerializer, CartSerializer, OrderSerializer, ProductSerializer, PromoCodeSerializer
from .utils import get_current_cart, get_or_create_session


class DocsView(TemplateView):
    template_name = 'docs.html'


class ProductViewSet(ReadOnlyModelViewSet):
    serializer_class = ProductSerializer
    queryset = Product.objects.all()

    def get_serializer_class(self):
        if self.action in ('add_to_cart', 'remove_from_cart'):
            return CartItemSerializer
        return super().get_serializer_class()


class OrderViewSet(CreateModelMixin, ReadOnlyModelViewSet):
    serializer_class = OrderSerializer
    queryset = Order.objects.all()

    def filter_queryset(self, queryset):
        session = get_or_create_session(self.request)
        return super().filter_queryset(queryset).filter(cart__session_key=session.session_key)

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        return response


class CartAPIView(GenericAPIView):
    queryset = Cart.objects.all()
    serializer_class = CartSerializer

    def get(self, request, *args, **kwargs):
        cart = get_current_cart(request)
        serializer = self.serializer_class(cart)
        return Response(serializer.data, status=status.HTTP_200_OK)


class CartItemsViewSet(CreateModelMixin, DestroyModelMixin, ReadOnlyModelViewSet):
    queryset = CartItem.objects.all()
    serializer_class = CartItemSerializer

    def filter_queryset(self, queryset):
        cart = get_current_cart(self.request)
        return super().filter_queryset(queryset).filter(cart=cart)


class PromoCodesViewSet(CreateModelMixin, ReadOnlyModelViewSet):
    queryset = PromoCode.objects.all()
    serializer_class = PromoCodeSerializer

    def filter_queryset(self, queryset):
        cart = get_current_cart(self.request)
        return super().filter_queryset(queryset).filter(cart=cart)


