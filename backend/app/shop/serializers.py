from rest_framework.exceptions import ValidationError
from rest_framework.serializers import ModelSerializer, CharField
from django.shortcuts import get_object_or_404

from .models import Cart, CartItem, Order, Product, PromoCode
from .utils import get_current_cart


class ProductSerializer(ModelSerializer):
    class Meta:
        model = Product
        fields = ('id', 'name', 'image', 'price', 'description', 'quantity')


class CartItemSerializer(ModelSerializer):
    class Meta:
        model = CartItem
        fields = ('id', 'product', 'quantity')

    def create(self, validated_data):
        cart = get_current_cart(self.context.get("request"))
        qs = cart.items.filter(product=validated_data['product'])
        if qs:
            existing_item = qs.first()
            existing_item.quantity = validated_data['quantity']
            existing_item.save()
            return existing_item
        new_cart_item = CartItem.objects.create(**validated_data)
        cart.items.add(new_cart_item)
        return new_cart_item


class DetailedCartItemSerializer(ModelSerializer):
    product = ProductSerializer()

    class Meta:
        model = CartItem
        fields = ('id', 'quantity', 'product', 'promo_price', 'total_price', 'total_price', 'promo_price')


class PromoCodeSerializer(ModelSerializer):
    code = CharField()

    class Meta:
        model = PromoCode
        fields = ('id', 'code', 'value', 'expire_date')
        read_only_fields = ('value', 'expire_date')

    def validate_code(self, data):
        return data

    def create(self, validated_data):
        code = get_object_or_404(PromoCode, code=validated_data['code'])
        cart = get_current_cart(self.context.get("request"))
        cart.validated_promo_codes.add(code)
        cart.save()
        return code


class CartSerializer(ModelSerializer):
    items = DetailedCartItemSerializer(many=True)
    validated_promo_codes = PromoCodeSerializer(many=True)

    class Meta:
        model = Cart
        fields = ('items', 'validated_promo_codes', 'total_price')


class OrderSerializer(ModelSerializer):
    cart = CartSerializer(read_only=True)

    class Meta:
        model = Order
        fields = ('id', 'cart', 'email',)
        read_only_fields = ('cart',)

    def create(self, validated_data):
        cart = get_current_cart(self.context.get("request"))
        if not cart.items.all():
            raise ValidationError('You cart is empty')
        cart.pending = True
        cart.save()
        validated_data.update({'cart': cart})
        return Order.objects.create(**validated_data)
