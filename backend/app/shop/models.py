from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils.timezone import now

from .logic import apply_promo_code, create_promo_code, get_promo_code_expire_date


class Product(models.Model):
    name = models.CharField(max_length=255, null=False, blank=False)
    image = models.ImageField()
    price = models.DecimalField(null=False, blank=False, validators=[MinValueValidator(0), ], decimal_places=2,
                                max_digits=100)
    description = models.TextField(null=False, blank=False)
    quantity = models.IntegerField(null=False, blank=False, default=0, validators=[MinValueValidator(0)], )


class CartItem(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField(null=False, blank=False, validators=[MinValueValidator(1)], )

    @property
    def promo_price(self):
        return format(apply_promo_code(self), '.2f')

    @property
    def total_price(self):
        total_price = float(self.promo_price) * self.quantity
        return format(total_price, '.2f')


class ProductCategory(models.Model):
    name = models.CharField(max_length=255, null=False, blank=False)
    products = models.ManyToManyField(Product, related_name='category')


class PromoCode(models.Model):
    category = models.ManyToManyField(ProductCategory, related_name='promo_codes')
    code = models.CharField(max_length=255, null=False, blank=False, unique=True, default=create_promo_code())
    value = models.IntegerField(null=False, blank=False, default=0,
                                validators=[MinValueValidator(0), MaxValueValidator(100)])
    expire_date = models.DateTimeField(default=get_promo_code_expire_date)

    @property
    def is_expired(self):
        return self.expire_date < now()


class Cart(models.Model):
    items = models.ManyToManyField(CartItem, related_name='cart', blank=False)
    session_key = models.CharField(max_length=255, null=False, blank=False)
    realized = models.BooleanField(null=False, blank=False, default=False)
    pending = models.BooleanField(default=False)
    validated_promo_codes = models.ManyToManyField(PromoCode, null=True, blank=True, related_name='cart')

    @property
    def total_price(self):
        return format(sum(float(i.total_price) for i in self.items.all()), '.2f')


class Order(models.Model):
    cart = models.ForeignKey(Cart, blank=False, related_name='order', on_delete=models.CASCADE)
    email = models.EmailField(null=False, blank=False, unique=True)
