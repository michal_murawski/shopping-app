from django.urls import include, path

urlpatterns = [
    path('', include('app.shop.urls', namespace='shop'), name="shop"),
]
