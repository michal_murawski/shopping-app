import os

from settings.common import *  # flake8:noqa

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DEBUG = True
INCLUDE_DOCS_URLS = True
ALLOWED_HOSTS = ['*']

STATIC_ROOT = BASE_DIR + '/static'
MEDIA_ROOT = BASE_DIR + '/media'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
